#!/usr/bin/env python3

from ideep_test import _onednn_graph as og
import numpy as np

if __name__ == '__main__':
    e = og.engine(og.engine.kind.cpu, 0)
    print(e)
    lt = og.logical_tensor(100, og.logical_tensor.data_type.f32, [3], [1], og.logical_tensor.property_type.variable)
    a_ = np.array([1., 2., 3.], np.float32)
    b_ = np.array([4., 5., 6.], np.float32)
    a = og.tensor(lt, e, a_.data).to_numpy(lt)
    b = og.tensor(lt, e, b_.data).to_numpy(lt)
    print(a, b)
    assert np.array_equal(a, a_) and np.array_equal(b, b_)
