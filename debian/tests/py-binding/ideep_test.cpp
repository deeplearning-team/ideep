#include <pybind11/pybind11.h>

namespace py = pybind11;

#include <ideep/python/binding.hpp>

PYBIND11_MODULE(ideep_test, m) {
    ideep::initOnednnGraphPythonBindings(m.ptr());
    m.attr("__version__") = "dev";
}
