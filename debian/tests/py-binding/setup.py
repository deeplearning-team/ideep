#!/usr/bin/env python3

from pybind11.setup_helpers import Pybind11Extension
from setuptools import setup

__version__ = "0.0.1"

ext_modules = [
    Pybind11Extension(
        "ideep_test",
        ["ideep_test.cpp"],
        cxx_std=11,
        extra_link_args=["-ldnnl"],
    ),
]

setup(
    name="ideep_test",
    version=__version__,
    author="Shengqi Chen",
    author_email="harry-chen@outlook.com",
    description="Test Python binding of ideep",
    ext_modules=ext_modules,
    zip_safe=False,
    python_requires=">=3.11",
)
